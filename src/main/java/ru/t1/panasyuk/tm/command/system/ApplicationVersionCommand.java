package ru.t1.panasyuk.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String DESCRIPTION = "Show version info.";

    @NotNull
    public static final String NAME = "version";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.24.0");
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}